const { makeModel } = require('../src/model');
const test = require('tape');

test('single node, single group', function (t) {
  t.plan(3);

  let data = makeModel({
    groups: {
      1: {}
    },
    nodes: [
      { g: 1, o: 1, oidx: 2, icon: 'img', spc: [] }
    ],
    constants: {
      orbitRadii: [],
      skillsPerOrbit: []
    }
  });

  t.equal(data.length, 1)
  t.equal(typeof data[ 0 ], 'object')
  t.equal(typeof data[ 0 ].getGroup(), 'object')
})