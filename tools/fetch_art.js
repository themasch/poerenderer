const http = require('https'),
  fs = require('fs'),
  mkdir = require('mkdirp'),
  Path = require('path');

const data = require('../data');

const ICON_BASE_URL = 'https://web.poecdn.com/image/';
const ALREADY_LOADING = [];

function fetchNodeImage (node, file) {
  if (ALREADY_LOADING[ node.icon ] === undefined) {
    ALREADY_LOADING[ node.icon ] = new Promise((resolve, reject) => {
      http.get(ICON_BASE_URL + node.icon, res => {
        const { statusCode } = res;
        if (statusCode !== 200) {
          console.error(`Request failed: Status Code: ${statusCode}, url: ${node.icon}`);
          res.resume();
          return reject(result);
        }

        console.log('writing ' + node.icon)
        res.pipe(file);
        res.on('end', () => resolve(node.icon));
      });
    })
  }

  return ALREADY_LOADING[ node.icon ];
}

function createNodeImageFile (node) {
  const path = Path.join(__dirname, '../', node.icon);
  const dirname = Path.dirname(path);
  return new Promise((resolve, reject) => {
    mkdir(dirname, (err) => {
      if (err) {
        return reject;
      }

      resolve(fs.createWriteStream(path))
    })
  });

}

function checkNodeExists (node) {
  const path = Path.join(__dirname, '../', node.icon);
  return new Promise(resolve => fs.access(path, fs.constants.R_OK | fs.constants.F_OK, (err) => resolve(!err)))
}

function getNodeImage (node) {
  return checkNodeExists(node)
    .then(exists => {
      if (!exists) {
        return createNodeImageFile(node).then(fileStream => fetchNodeImage(node, fileStream))
      }

      return node.icon;
    })
    .then(name => console.log(name + ' exists'))
    .catch(err => console.error(err))
}

Promise.all(data.nodes.map(getNodeImage));