// die winkel für den große orbit werden anders berechnet als andere
// i => getOrbitAngle(x, 12) + y * 017453293. die map unten ist i => [x, y]
const ORBIT_ANGLE_MAP = [
  [ 0, 0 ],
  [ 0, 10 ],
  [ 0, 20 ],
  [ 1, 0 ],
  [ 1, 10 ],
  [ 1, 15 ],
  [ 1, 20 ],
  [ 2, 0 ],
  [ 2, 10 ],
  [ 2, 20 ],
  [ 3, 0 ],
  [ 3, 10 ],
  [ 3, 20 ],
  [ 4, 0 ],
  [ 4, 10 ],
  [ 4, 15 ],
  [ 4, 20 ],
  [ 5, 0 ],
  [ 5, 10 ],
  [ 5, 20 ],
  [ 6, 0 ],
  [ 6, 10 ],
  [ 6, 20 ],
  [ 7, 0 ],
  [ 7, 10 ],
  [ 7, 15 ],
  [ 7, 20 ],
  [ 8, 0 ],
  [ 8, 10 ],
  [ 8, 20 ],
  [ 9, 0 ],
  [ 9, 10 ],
  [ 9, 20 ],
  [ 10, 0 ],
  [ 10, 10 ],
  [ 10, 15 ],
  [ 10, 20 ],
  [ 11, 0 ],
  [ 11, 10 ],
  [ 11, 20 ],
];

class Group {
  constructor (x, y) {
    this.x = x;
    this.y = y;
    this.nodes = [];
  }

  addNode (node) {
    this.nodes.push(node);
  }

  getGlobalPosition () {
    return { x: this.x, y: this.y };
  }
}

class Node {

  constructor (data) {
    this.data = data;
  }

  getImageUrl() {
    return this.data.image;
  }

  getGroup () {
    return this.data.group;
  }

  getOrbitRadius () {
    return this.data.orbit.radius;
  }

  getOrbitAngle () {
    return this.data.orbit.angle;
  }

  getGlobalPosition () {
    let center = this.getGroup().getGlobalPosition();
    let orbit = this.getOrbitRadius();
    let a = this.getOrbitAngle();

    return {
      x: center.x + orbit * Math.cos(a),
      y: center.y + orbit * Math.sin(a)
    }
  }

  getRadius () {
    if (this.data.isNoteable) {
      return 70;  //nodeRadiusNoteable
    }
    if (this.data.isKeystone) {
      return 109; //nodeRadiusKeystone
    }
    if (this.data.isJewelSocket) {
      return 70; //nodeRadiusJewel
    }
    if (this.data.isMastery) {
      return 107; //nodeRadiusMastery;
    }
    if (this.data.isClassStart) {
      return 200; //nodeRadiusClassStart;
    }

    return 51; //nodeRadius;
  }
}

function makeGroup (groupData) {
  return new Group(groupData.x, groupData.y)
}

function getOrbitAngle (e, t) {
  const n = .017453293;
  if (t === 40) {
    let [ a, b ] = ORBIT_ANGLE_MAP[ e ];
    return getOrbitAngle(a, 12) + b * n;
  }

  return 2 * Math.PI * e / t
}

function makeNode (groups, rawData, nodeData) {
  let group = groups[ nodeData.g ];

  let orbitRadius = rawData.constants.orbitRadii[ nodeData.o ];
  let orbitMaxCount = rawData.constants.skillsPerOrbit[ nodeData.o ];
  let orbitAngle = getOrbitAngle(nodeData.oidx, orbitMaxCount) - 0.5 * Math.PI
  let node = new Node({
    group: group,
    orbit: {
      radius: orbitRadius,
      angle: orbitAngle
    },
    image: nodeData.icon,
    ascendancyName: nodeData.ascendancyName,
    isAscendancyStart: nodeData.isAscendancyStart,
    isJewelSocket: nodeData.isJewelSocket,
    isMultipleChoice: nodeData.isMultipleChoice,
    isMultipleChoiceOption: nodeData.isMultipleChoiceOption,
    isClassStart: nodeData.spc.length !== 0,
    isKeystone: nodeData.ks,
    isMastery: nodeData.m,
    isNoteable: nodeData.not,
  });
  group.addNode(node);
  return node;
}

function makeModel (passiveData) {
  // step 1: build a basic list of groups
  const groups = {};
  for (let groupdId in passiveData.groups) {
    if (!passiveData.groups.hasOwnProperty(groupdId)) continue;

    groups[ groupdId ] = makeGroup(passiveData.groups[ groupdId ], passiveData);
  }

  // step 2: create nodes, add groups to nodes and vice versa
  return passiveData.nodes.map(makeNode.bind(this, groups, passiveData))
}

exports.makeModel = makeModel;
exports.Node = Node;
exports.Group = Group;