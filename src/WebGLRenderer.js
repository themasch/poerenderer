const THREE = require('three');
const CameraControls = require('./CameraControls');

class ImageCache {
  constructor (loadingManager) {
    this.images = [];
    this.loadingManager = loadingManager;
  }

  load(url) {
    return new THREE.TextureLoader(this.loadingManager).load(url);
  }

  get(url) {
    if (!this.images[url]) {
      this.images[url] = this.load(url);
    }

    return this.images[url];
  }
}

class GLNode {
  /**
   *
   * @param {Node} node
   * @param {ImageCache} imageCache
   */
  constructor (node, imageCache) {
    this.node = node;
    this.image = imageCache.get(node.getImageUrl());
  }

  getObject () {
    if (!this._object) {
      let geometry = new THREE.CircleBufferGeometry(this.node.getRadius(), 20);
      let material = new THREE.MeshBasicMaterial({ map: this.image });
      this._object = new THREE.Mesh(geometry, material);
      let pos = this.node.getGlobalPosition();
      this._object.position.x = pos.x;
      this._object.position.y = pos.y * -1;
      if (this.node.data.isMultipleChoice) {
        this._object.position.z = 10.5;
      }
    }

    return this._object;
  }
}

class WebGLRenderer {
  constructor () {
    this.loadingManager = new THREE.LoadingManager(
      () => this.render(),
      (url, itemsLoaded, itemsTotal) => {},
      err => console.error('ERROR', err)
    );
    this.imageCache = new ImageCache(this.loadingManager);
    this.nodes = [];
  }

  addNode (node) {
    const glNode = new GLNode(node, this.imageCache);
    this.nodes.push(glNode);
    if (this._scene) {
      this._scene.add(glNode);
    }
  }

  createRenderer(container) {
    const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 10000)
    const renderer = new THREE.WebGLRenderer({
      antialias: true
    })
    renderer.setSize( window.innerWidth, window.innerHeight );

    new CameraControls(camera, renderer.domElement)
    camera.position.z = 1000;

    this._renderer = renderer;
    this._camera = camera;

    container.appendChild(renderer.domElement);
  }

  createScene() {
    const scene = new THREE.Scene()
    this.nodes.forEach(node => scene.add(node.getObject()));

    return scene;
  }

  render() {
    if (!this._scene) {
      this._scene = this.createScene();
    }


    this._renderer.render( this._scene, this._camera );
  }
}

module.exports = WebGLRenderer;