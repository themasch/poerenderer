const { Vector3 } = require('three');

class CameraControls {
  /**
   *
   * @param {THREE.Camera} camera
   * @param {HTMLElement} domElement
   */
  constructor (camera, domElement) {
    this.camera = camera;
    this.center = new Vector3();
    this.domElement = domElement;
    this.zoomFactor = 0.02;
    domElement.addEventListener('wheel', this.onMouseWheel.bind(this), false);
    domElement.addEventListener('mousedown', this.onMouseDown.bind(this), false)
    domElement.addEventListener('mouseup', () => domElement.removeEventListener('mousemove', this.onMouseMove));
    domElement.addEventListener('contextmenu', (event) => {
      event.preventDefault();
      return false;
    }, false)
    this.onMouseMove = this.onMouseMove.bind(this);
  }

  /**
   *
   * @param {number} deltaY
   */
  zoom (deltaY) {
    let factor = 1 + (deltaY / 500);

    let newZ = Math.ceil(this.camera.position.z * factor);
    if (newZ > 9000 || newZ < 800) {
      return;
    }

    this.camera.position.z = newZ;
  }

  pan (movement) {
    this.camera.position.add(movement.multiplyScalar(this.camera.position.z / 500));
  }

  /**
   *
   * @param {MouseEvent} mouseEvent
   */
  onMouseMove (mouseEvent) {
    this.pan(new Vector3(-mouseEvent.movementX, mouseEvent.movementY))
  }

  /**
   *
   * @param {MouseEvent} mouseEvent
   */
  onMouseDown (mouseEvent) {
    if (mouseEvent.button === 0) {
      this.domElement.addEventListener('mousemove', this.onMouseMove, false);
    }
  }

  /**
   *
   * @param {MouseWheelEvent} wheelEvent
   */
  onMouseWheel (wheelEvent) {
    this.zoom(wheelEvent.deltaY)
  }
}

module.exports = CameraControls;