const WebGLRenderer = require('./WebGLRenderer');
const { makeModel } = require('./model');
const Stats = require('stats.js');
const renderer = new WebGLRenderer();

const stats = new Stats();
document.body.appendChild( stats.dom );
function animationLoop() {
  requestAnimationFrame(function() {
    stats.begin();
    renderer.render();
    stats.end();
    animationLoop();
  });
}

fetch('./data.json')
  .then(response => response.json())
  .then(makeModel)
  .then(nodes => {
    nodes.map(renderer.addNode.bind(renderer))
  })
  .then(() => {
    renderer.createRenderer(document.body);
    animationLoop();
  })